<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Exceptions;

/**
 * Class SyncClientRemoteBusyException.
 *
 * This exception is thrown when the remote responds with error codes indicating
 * it is (very) temporary too busy and unable to process the request, but the
 * request should be retried.
 */
final class SyncClientRemoteBusyException extends \Exception {}
