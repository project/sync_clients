<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Exceptions;

/**
 * Class SyncClientPausedException.
 *
 * This exception is thrown when the Sync Client has been paused and request to
 * the remote are attempted. This excludes internal checks to see if the Sync
 * Client can be unpaused.
 *
 * All code using the Sync Client, should handle this exception appropriately.
 */
final class SyncClientPausedException extends \Exception {}
