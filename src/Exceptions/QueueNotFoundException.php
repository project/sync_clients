<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Exceptions;

/**
 * Class QueueNotFoundException.
 *
 * This exception is thrown when trying to add an Entity to a queue and the
 * Queue cannot be found.
 */
final class QueueNotFoundException extends \Exception {}
