<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Exceptions;

/**
 * Class SyncClientUnknownException.
 *
 * Thrown when the Sync Client encounters exception is does not know how to
 * handle. Includes the previous error.
 */
final class SyncClientUnknownException extends \Exception {}
