<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Exceptions;

/**
 * Class SyncClientNotFoundException.
 *
 * This exception is thrown when the plugin manager cannot find a plugin for
 * given plugin, queue or job type ID.
 */
final class SyncClientNotFoundException extends \Exception {}
