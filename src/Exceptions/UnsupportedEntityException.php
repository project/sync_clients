<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Exceptions;

/**
 * Class UnsupportedEntityException.
 *
 * This exception is thrown when a Sync Client cannot convert given entity into
 * a Job payload array.
 */
final class UnsupportedEntityException extends \Exception {}
