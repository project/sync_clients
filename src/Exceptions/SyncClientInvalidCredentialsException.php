<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Exceptions;

/**
 * Class SyncClientInvalidCredentialsException.
 *
 * This exception is thrown when the current Sync Client credentials for the
 * remote are incorrect.
 */
final class SyncClientInvalidCredentialsException extends \Exception {}
