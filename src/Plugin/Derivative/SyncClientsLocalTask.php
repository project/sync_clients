<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\sync_clients\SyncClient\SyncClientManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local task definitions for all Sync Client plugin settings.
 */
final class SyncClientsLocalTask extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\sync_clients\SyncClient\SyncClientManagerInterface $syncClientManager
   *   The Sync Client Manager.
   */
  public function __construct(protected SyncClientManagerInterface $syncClientManager) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): static {
    return new static(
      $container->get('plugin.manager.sync_clients.sync_client_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    /** @var array $plugin */
    foreach ($this->syncClientManager->getDefinitions() as $plugin_id => $plugin) {
      $this->derivatives["sync_clients.settings.$plugin_id"] = [
        'title' => $plugin['label'],
        'base_route' => 'sync_clients.settings',
        'route_name' => 'sync_clients.settings.plugin',
        'route_parameters' => [
          'sync_client_plugin' => $plugin_id,
        ],
      ];
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
