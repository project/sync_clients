<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sync_clients\SyncClient\SyncClientInterface;
use Drupal\sync_clients\SyncClient\SyncClientManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Advanced Queue Sync Job Types.
 */
abstract class SyncJobBase extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The Sync Client.
   *
   * @var \Drupal\sync_clients\SyncClient\SyncClientInterface|null
   */
  protected ?SyncClientInterface $syncClient;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\sync_clients\SyncClient\SyncClientManagerInterface $syncClientManager
   *   The Sync Client Manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected SyncClientManagerInterface $syncClientManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    try {
      $this->syncClient = $syncClientManager->getPluginByJobTypeId($plugin_id);
    }
    catch (\Exception $exception) {
      $this->syncClient = NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.sync_clients.sync_client_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createJobFingerprint(Job $job): string {

    // Filter the payload by the data keys defined on the Sync Client.
    $payload = $job->getPayload();
    $payload = array_intersect_key($payload, array_flip($this->syncClient->duplicateDetectionQueueItemPayloadKeys()));

    // This is same as
    // \Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase::createJobFingerprint().
    $data = $job->getQueueId() . $job->getType() . serialize($payload);
    return hash('tiger128,3', $data);
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job): JobResult {

    if (is_null($this->syncClient)) {
      // Return as hard-fail: no retries.
      return JobResult::failure('No Sync Client found for this job type.', 0, 0);
    }

    // We return success, as at this level all is OK.
    return JobResult::success();
  }

}
