<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Mail;

/**
 * Handles the assembly and dispatch of HTML emails.
 */
interface MailHandlerInterface {

  /**
   * Sends an email to a user.
   *
   * @param string $to
   *   The address the email will be sent to. Must comply with RFC 2822.
   * @param string $subject
   *   The subject. Must not contain any newline characters.
   * @param array $body
   *   A render array representing the message body.
   * @param array $params
   *   Email parameters. Recognized keys:
   *     - id: A unique identifier of the email type.
   *       Allows hook_mail_alter() implementations to identify specific emails.
   *       Defaults to "mail".
   *     - cc: The CC address or addresses (separated by a comma). No default.
   *     - bcc: The BCC address or addresses (separated by a comma). No default.
   *
   * @return bool
   *   TRUE if the email was sent successfully, FALSE otherwise.
   */
  public function sendMail(string $to, string $subject, array $body, array $params = []): bool;

}
