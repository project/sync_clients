<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Mail;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Basic Mail Handler for sending internal warning, error mails.
 */
final class MailHandler implements MailHandlerInterface {

  /**
   * The site config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $configSite;

  /**
   * Constructs a new MailHandler object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected LanguageManagerInterface $languageManager,
    protected LoggerInterface $logger,
    protected MailManagerInterface $mailManager,
  ) {
    $this->configSite = $configFactory->get('system.site');
  }

  /**
   * {@inheritdoc}
   */
  public function sendMail(string $to, string $subject, array $body, array $params = []): bool {

    $default_params = [
      'headers' => [
        'Content-Type' => 'text/html; charset=UTF-8;',
        'Content-Transfer-Encoding' => '8Bit',
      ],
      'id' => 'mail',
      'from' => $this->configSite->get('mail'),
      'reply-to' => NULL,
      'subject' => $subject,
      'langcode' => $this->languageManager->getCurrentLanguage()->getId(),
      'body' => $body,
    ];

    if (isset($params['id'])) {
      $default_params['id'] = $params['id'];
    }

    if (isset($params['cc'])) {
      $default_params['headers']['Cc'] = $params['cc'];
    }

    if (isset($params['bcc'])) {
      $default_params['headers']['Bcc'] = $params['bcc'];
    }

    $message = $this->mailManager->mail('sync_clients', $default_params['id'], $to, $default_params['langcode'], $default_params, $default_params['reply-to']);

    $context = [
      'id' => $default_params['id'],
      'subject' => $subject,
      'to' => $to,
      'params' => serialize($default_params),
    ];

    $sent = (bool) $message['result'];
    if ($sent === FALSE) {
      $this->logger->error('Failed to sent "{id}" mail to {to} with "{subject}", message "{body}" and parameters: {params}.', $context);
    }

    return $sent;
  }

}
