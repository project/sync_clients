<?php

declare(strict_types=1);

namespace Drupal\sync_clients;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\sync_clients\Exceptions\QueueNotFoundException;
use Drupal\sync_clients\SyncClient\SyncClientManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Default implementation of QueueHelperInterface.
 */
class QueueHelper implements QueueHelperInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\sync_clients\SyncClient\SyncClientManagerInterface $syncClientManager
   *   The Sync Client Manager.
   */
  public function __construct(
    protected Connection $database,
    protected LoggerInterface $logger,
    protected SyncClientManagerInterface $syncClientManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function addToQueue(
    string $job_type_id,
    ?EntityInterface $entity = NULL,
    array $payload_extra = [],
    int $delay = 0,
  ): ?Job {

    $sync_client = $this->syncClientManager->getPluginByJobTypeId($job_type_id);
    $queue_id = $sync_client->getQueueId();

    $queue = Queue::load($queue_id);
    if (is_null($queue)) {
      throw new QueueNotFoundException("Queue $queue_id not found, for Sync Client {$sync_client->getLabel()}");
    }

    $payload = $sync_client->buildQueueItemPayload($entity, $payload_extra);
    $job = Job::create($job_type_id, $payload);
    $queue->enqueueJob($job, $delay);

    return $job;
  }

}
