<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a Sync Client plugin.
 *
 * @Annotation
 */
final class SyncClient extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public TranslatableMarkup $label;

  /**
   * The priority of the plugin.
   *
   * @var int
   */
  public int $priority;

  /**
   * The (Advanced Queue) Queue ID.
   *
   * The Queue ID that is to be used this Sync Client.
   *
   * @var string
   */
  public string $queue_id;

  /**
   * The (Advanced Queue) Job Type ID.
   *
   * The job type that is to be used by this Sync Client.
   *
   * @var string
   */
  public string $job_type_id;

}
