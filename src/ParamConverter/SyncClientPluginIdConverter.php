<?php

declare(strict_types=1);

namespace Drupal\sync_clients\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\sync_clients\Exceptions\SyncClientNotFoundException;
use Drupal\sync_clients\SyncClient\SyncClientManagerInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides upcasting for a Sync Client plugin.
 */
final class SyncClientPluginIdConverter implements ParamConverterInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\sync_clients\SyncClient\SyncClientManagerInterface $syncClientManager
   *   The Sync Client Manager.
   */
  public function __construct(protected SyncClientManagerInterface $syncClientManager) {}

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {

    try {
      return $this->syncClientManager->getPluginById($value);
    }
    catch (SyncClientNotFoundException $exception) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    return (isset($definition['type']) && $definition['type'] === 'sync_client_plugin');
  }

}
