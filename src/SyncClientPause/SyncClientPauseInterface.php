<?php

declare(strict_types=1);

namespace Drupal\sync_clients\SyncClientPause;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines Sync Client Pause object.
 */
interface SyncClientPauseInterface {

  /**
   * The pause cause 'forced'.
   *
   * To be used when pause state forced by the error level raising mechanism or
   * other (fatal) errors indicating that any future requests will also fail.
   * When active, the pause state should only be manually undone.
   *
   * @var string
   */
  public const PAUSE_CAUSE_FORCED = 'forced';

  /**
   * The pause cause 'maintenance'.
   *
   * Is used when Drupal's Maintenance Mode is active.
   * Should only be used by isPaused().
   *
   * @var string
   */
  public const PAUSE_CAUSE_MAINTENANCE = 'maintenance';

  /**
   * The pause cause 'manual'.
   *
   * To be used when pause state is manually set.
   * When active, the pause state should only be manually undone.
   *
   * @var string
   */
  public const PAUSE_CAUSE_MANUAL = 'manual';

  /**
   * The pause cause 'sync error level 1'.
   *
   * To be used on first error that cannot automaticly be resolved.
   * This should trigger PAUSE_DURATION_LVL_1 to be stored as expirable
   * key-value, and when it has expired, a new connection test should be made.
   * If the test success, the pause state must be lifted.
   * If that fails again, the function implementing the level raising
   * mechanism should raise 'sync error level 2'.
   *
   * @var string
   */
  public const PAUSE_CAUSE_ERROR_LVL_1 = 'sync_error_lvl_1';

  /**
   * The pause cause 'sync error level 2'.
   *
   * Continuation of 'sync error level 1'.
   * Should only be set by the function implementing the level raising
   * mechanism.
   *
   * @var string
   */
  public const PAUSE_CAUSE_ERROR_LVL_2 = 'sync_error_lvl_2';

  /**
   * The pause cause 'sync error level 3'.
   *
   * Continuation of 'sync error level 2'.
   * However, on connection test failure, the function implementing the level
   * raising mechanism must set the cause to 'forced'.
   * This should also be accompanied by an error mail to the relevant persons.
   *
   * @var string
   */
  public const PAUSE_CAUSE_ERROR_LVL_3 = 'sync_error_lvl_3';

  /**
   * Get the cause.
   *
   * @return string
   *   The pause cause.
   */
  public function getCause(): string;

  /**
   * Message with pause cause and reason.
   *
   * For status messages or inclusion in mails.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The message.
   */
  public function getPauseMessage(): TranslatableMarkup;

  /**
   * Check if the pause was forced.
   *
   * @return bool
   *   TRUE if the cause equals PAUSE_CAUSE_FORCED.
   */
  public function isForced(): bool;

  /**
   * Check if the pause is due to maintenance.
   *
   * @return bool
   *   TRUE if the cause equals PAUSE_CAUSE_MAINTENANCE.
   */
  public function isMaintenance(): bool;

  /**
   * Check if the pause was manually set.
   *
   * @return bool
   *   TRUE if the cause equals PAUSE_CAUSE_MANUAL.
   */
  public function isManual(): bool;

}
