<?php

declare(strict_types=1);

namespace Drupal\sync_clients\SyncClientPause;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements Sync Client Pause object.
 *
 * Allows for easier handling/passing on of Sync Client Pause information.
 */
final class SyncClientPause implements SyncClientPauseInterface {

  use StringTranslationTrait;

  /**
   * Reason fallback value.
   *
   * @var string
   */
  private const REASON_DEFAULT = 'N/A';

  /**
   * The pause cause.
   *
   * @var string
   */
  protected string $cause;

  /**
   * The pause reason.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected TranslatableMarkup $reason;

  /**
   * SyncClientPause constructor.
   *
   * @param string $cause
   *   The pause cause. Any of the PAUSE_CAUSE_[X] constants from
   *   SyncClientPauseInterface.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $reason
   *   Optional, but recommended, reason for why the sync was paused.
   *
   * @throws \Exception
   */
  public function __construct(string $cause, ?TranslatableMarkup $reason = NULL) {

    if (!in_array($cause, [
      self::PAUSE_CAUSE_FORCED,
      self::PAUSE_CAUSE_MAINTENANCE,
      self::PAUSE_CAUSE_MANUAL,
      self::PAUSE_CAUSE_ERROR_LVL_1,
      self::PAUSE_CAUSE_ERROR_LVL_2,
      self::PAUSE_CAUSE_ERROR_LVL_3,
    ], TRUE)) {
      throw new \Exception('Unknown cause: ' . $cause);
    }

    if (is_null($reason)) {
      $reason = new TranslatableMarkup('@reason', ['@reason' => self::REASON_DEFAULT]);
    }

    $this->cause = $cause;
    $this->reason = $reason;
  }

  /**
   * {@inheritdoc}
   */
  public function getCause(): string {
    return $this->cause;
  }

  /**
   * {@inheritdoc}
   */
  public function getPauseMessage(): TranslatableMarkup {

    $params = [
      '@cause' => $this->cause,
      '@reason' => $this->reason,
    ];

    return match ($this->cause) {
      self::PAUSE_CAUSE_FORCED => $this->t('Sync has been forcefully paused. Reason for last pause trigger: @reason', $params),
      self::PAUSE_CAUSE_MANUAL => $this->t('@reason', $params),
      default => $this->t('Sync is paused due to "@cause" and reason: @reason', $params),
    };
  }

  /**
   * {@inheritdoc}
   */
  public function isForced(): bool {
    return $this->cause === self::PAUSE_CAUSE_FORCED;
  }

  /**
   * {@inheritdoc}
   */
  public function isMaintenance(): bool {
    return $this->cause === self::PAUSE_CAUSE_MAINTENANCE;
  }

  /**
   * {@inheritdoc}
   */
  public function isManual(): bool {
    return $this->cause === self::PAUSE_CAUSE_MANUAL;
  }

}
