<?php

declare(strict_types=1);

namespace Drupal\sync_clients;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\sync_clients\AdvancedQueue\Processor;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Service manipulations.
 */
final class SyncClientsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {

    // Replace the Advanced Queue Processor with our extent + add the Sync
    // Client Manager.
    $container->getDefinition('advancedqueue.processor')
      ->setClass(Processor::class)
      ->addArgument(new Reference('plugin.manager.sync_clients.sync_client_manager'));
  }

}
