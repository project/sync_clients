<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Form;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\sync_clients\SyncClientPause\SyncClientPauseInterface;

/**
 * Sync Client pause mode form section.
 */
trait PluginConfigFormPauseModeTrait {

  use StringTranslationTrait;

  /**
   * Add the mail notification fields.
   *
   * @param array $form
   *   The form.
   */
  public function addPauseModeFields(array &$form): void {

    $button_base = [
      '#type' => 'submit',
      '#button_type' => 'secondary',
      '#id' => 'pause_toggle',
      '#attributes' => [
        'class' => [
          'form-item',
        ],
      ],
      '#limit_validation_errors' => [],
    ];

    if (!is_null($this->syncClient->isPaused(TRUE, TRUE))) {

      $form['state'] = [
        '#type' => 'details',
        '#title' => $this->t('Pause mode: ON'),
        '#description' => $this->t('Do not re-enable the sync without checking if the reason for the current pause state has been resolved!'),
        '#open' => TRUE,
      ];

      $form['state']['disable'] = $button_base + [
        '#value' => $this->t('Unpause sync'),
        '#submit' => ['::disablePauseMode'],
      ];
    }
    else {

      $form['state'] = [
        '#type' => 'details',
        '#title' => $this->t('Pause mode: OFF'),
        '#description' => $this->t('Disable the sync. Do not do this unless there is reason to halt all communications with the remote.'),
        '#open' => FALSE,
      ];

      $form['state']['enable'] = $button_base + [
        '#value' => $this->t('Pause sync'),
        '#submit' => ['::enablePauseMode'],
      ];
    }

    $form['state']['#weight'] = -100;
    $form['state']['error_levels'] = [
      '#type' => 'inline_template',
      '#template' => '<h4>{% trans %} Auto-pause mechanism and sync error levels {% endtrans %}</h4>
        <p>{% trans %} The sync has 3 error levels to keep track of connection/request issues to limit request which are likely to fail and avoid unnecessary processing.<br>
        As long as the highest level is not exceeded, the sync can auto-recover without manual intervention.{% endtrans %}</p>

        <h5>{% trans %} The procedure {% endtrans %}</h5>
        <p>{% trans %} The Every request failure raises the error level and causes the sync to go into pause for a duration corresponding to that level.<br>
        Once that duration is passed, a connection test will be made and on success the sync returns back to normal.<br>
        If the test fails, the error level is raised and the procedure repeated.{% endtrans %}</p>

        <p>{% trans %} When the connection test fails at level 3, the sync will be forcefully put it Pause Mode and can only be reset manually above.<br>
        However, when level 3 is reached a warning mail is sent. And when the sync is paused, an error mail is sent to the corresponding recipients below. {% endtrans %}</p>

        <h5>{% trans %} The error levels {% endtrans %}</h5>
        <h6>{% trans %} Level 1: {% endtrans %}</h6>
        <ul>
            <li>{% trans %} Duration: {% endtrans %} {% trans %} 1 min {% endtrans %}</li>
            <li>{% trans %} Mail: {% endtrans %} {% trans %} None {% endtrans %}</li>
        </ul>
        <h6>{% trans %} Level 2: {% endtrans %}</h6>
        <ul>
            <li>{% trans %} Duration: {% endtrans %} {% trans %} 5 min {% endtrans %}</li>
            <li>{% trans %} Mail: {% endtrans %} {% trans %} None {% endtrans %}</li>
        </ul>
        <h6>{% trans %} Level 3: {% endtrans %}</h6>
        <ul>
            <li>{% trans %} Duration: {% endtrans %} {% trans %} 10 min {% endtrans %}</li>
            <li>{% trans %} Mail: {% endtrans %} {% trans %} Warning {% endtrans %}</li>
        </ul>
        <h6>{% trans %} Forcefully paused {% endtrans %}</h6>
        <ul>
            <li>{% trans %} Duration: {% endtrans %} {% trans %} until manual unpause< {% endtrans %}/li>
            <li>{% trans %} Mail: {% endtrans %} {% trans %} Error {% endtrans %}</li>
        </ul>
       ',
      '#context' => [],
    ];
  }

  /**
   * Disable 'pause' mode.
   */
  public function disablePauseMode(): void {
    $this->syncClient->unPause();

    $subject = $this->t("Sync '@label' has been manually un-paused.", ['@label' => $this->syncClient->getLabel()]);
    $body = [
      $this->t('Manually un-paused by @username.', ['@username' => $this->currentUser->getDisplayName()]),
    ];

    $this->syncClient->sendNoticeMail($subject, $body);
  }

  /**
   * Enable 'pause' mode.
   */
  public function enablePauseMode(): void {
    $reason = $this->t('Manually paused by @username.', ['@username' => $this->currentUser->getDisplayName()]);
    $this->syncClient->pause(SyncClientPauseInterface::PAUSE_CAUSE_MANUAL, $reason);
  }

}
