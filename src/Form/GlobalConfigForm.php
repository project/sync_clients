<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sync_clients\SyncClient\SyncClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The global Sync Client config form.
 */
final class GlobalConfigForm extends ConfigFormBase {

  use ConfigFormMailNotificationsTrait;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $emailValidator
   *   The email validator.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected EmailValidatorInterface $emailValidator,
    protected $typedConfigManager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);

    $config_keys = $this->getEditableConfigNames();
    $this->config = $this->config(reset($config_keys));
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new GlobalConfigForm(
      $container->get('config.factory'),
      $container->get('email.validator'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    // @todo Add some info about these being the defaults.
    $form = parent::buildForm($form, $form_state);
    $this->addMailNotificationFields($form, $form_state);
    $form['mail']['#open'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      SyncClientInterface::CONFIG_KEY_DEFAULT,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sync_client_global_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $form_state->cleanValues();
    $this->config->setData($form_state->getValues())->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $this->validateMailNotificationFields($form, $form_state);
    parent::validateForm($form, $form_state);
  }

}
