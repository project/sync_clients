<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\sync_clients\SyncClient\SyncClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin Sync Client config form.
 */
final class PluginConfigForm extends ConfigFormBase {

  use ConfigFormMailNotificationsTrait;
  use PluginConfigFormPauseModeTrait;

  /**
   * The Sync Client plugin.
   *
   * @var \Drupal\sync_clients\SyncClient\SyncClientInterface
   */
  protected SyncClientInterface $syncClient;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $emailValidator
   *   The email validator.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typedConfigManager
   *   The typed config manager.
   */
  public function __construct(
    protected ConfigFactoryInterface $config_factory,
    protected EmailValidatorInterface $emailValidator,
    RouteMatchInterface $routeMatch,
    protected AccountInterface $currentUser,
    protected $typedConfigManager = NULL,
  ) {
    parent::__construct($config_factory, $typedConfigManager);

    // Plugin must be stored first, as we want to use it in
    // getEditableConfigNames().
    $this->syncClient = $routeMatch->getParameter('sync_client_plugin');

    $config_keys = $this->getEditableConfigNames();
    $this->config = $this->config(reset($config_keys));
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new PluginConfigForm(
      $container->get('config.factory'),
      $container->get('email.validator'),
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $this->addPauseModeFields($form);
    $this->addMailNotificationFields($form, $form_state);
    $this->syncClient->addPluginFields($form, $form_state);

    $form['actions']['#weight'] = 100;
    $form['mail']['#title'] = $this->t('Notification recipient overrides');
    $form['mail']['#description'] = $this->t('Leave empty to use the default recipients');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      $this->syncClient->getConfigKey(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sync_client_plugin_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $form_state->cleanValues();
    $this->config->setData($form_state->getValues())->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get title for the plugin form.
   *
   * @return string
   *   The title.
   */
  public function title(): string {
    return $this->syncClient->getLabel()->render();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#id'] !== 'pause_toggle') {

      $this->validateMailNotificationFields($form, $form_state);

      // Copy over values that where set in storage, like passwords to retain
      // their original values.
      $storage = $form_state->getStorage();
      $values = $form_state->getValues();
      $diff = array_intersect(array_keys($storage), array_keys($values));

      if (count($diff) > 0) {
        foreach ($diff as $key) {
          if (isset($values[$key]) && $values[$key] === '') {
            $values[$key] = $storage[$key];
          }
        }

        $form_state->setValues($values);
      }

      try {
        $this->syncClient->testConfig($values);
        $this->messenger()->addStatus($this->t('Connection OK.'));
      }
      catch (\Exception $exception) {
        $this->messenger()->addError($exception->getMessage());
      }

      parent::validateForm($form, $form_state);
    }
  }

}
