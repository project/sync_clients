<?php

declare(strict_types=1);

namespace Drupal\sync_clients\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\sync_clients\SyncClient\SyncClientBase;
use Drupal\sync_clients\SyncClient\SyncClientInterface;

/**
 * Sync Client mail notification config fields.
 */
trait ConfigFormMailNotificationsTrait {

  use StringTranslationTrait;

  /**
   * The Sync Client (plugin) config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * Add the mail notification fields.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addMailNotificationFields(array &$form, FormStateInterface $form_state): void {

    $form['mail'] = [
      '#type' => 'details',
      '#title' => $this->t('Notification recipients'),
      '#description' => $this->t('The default recipients for notifications'),
      '#open' => FALSE,
      '#weight' => -99,
    ];

    $form['mail'][SyncClientInterface::CONF_RECIPIENTS_ERROR] = [
      '#type' => 'textarea',
      '#title' => $this->t('Errors'),
      '#description' => $this->t('Recipients to receive error mails. One email address per line.'),
      '#default_value' => $this->config->get(SyncClientInterface::CONF_RECIPIENTS_ERROR) ?? '',
      '#weight' => 1,
    ];

    $form['mail'][SyncClientInterface::CONF_RECIPIENTS_WARNING] = [
      '#type' => 'textarea',
      '#title' => $this->t('Warnings'),
      '#description' => $this->t('Recipients to receive warning mails. One email address per line.'),
      '#default_value' => $this->config->get(SyncClientInterface::CONF_RECIPIENTS_WARNING) ?? '',
      '#weight' => 2,
    ];

    $form['mail'][SyncClientInterface::CONF_RECIPIENTS_NOTICE] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notices'),
      '#description' => $this->t('Recipients to receive notice mails. One email address per line.'),
      '#default_value' => $this->config->get(SyncClientInterface::CONF_RECIPIENTS_NOTICE) ?? '',
      '#weight' => 1,
    ];
  }

  /**
   * Validate the mail notification fields.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateMailNotificationFields(array &$form, FormStateInterface $form_state): void {

    $recipient_errors = [];
    $notification_types = [
      SyncClientInterface::CONF_RECIPIENTS_ERROR,
      SyncClientInterface::CONF_RECIPIENTS_WARNING,
      SyncClientInterface::CONF_RECIPIENTS_NOTICE,
    ];
    foreach ($notification_types as $type) {

      $recipients = $form_state->getValue($type);
      $recipients = SyncClientBase::configTextToArray($recipients);

      foreach ($recipients as $recipient) {
        if (!$this->emailValidator->isValid($recipient)) {
          $recipient_errors[$type][] = $recipient;
        }
      }
    }

    if (count($recipient_errors) > 0) {
      foreach ($recipient_errors as $type => $recipients) {
        $recipients = implode(' ', $recipients);
        $message = $this->t('Invalid recipients: @recipients', ['@recipients' => $recipients])->render();
        $form_state->setError($form['mail'][$type], $message);
      }
    }
  }

}
