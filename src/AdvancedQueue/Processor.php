<?php

declare(strict_types=1);

namespace Drupal\sync_clients\AdvancedQueue;

use Drupal\advancedqueue\Entity\QueueInterface;
use Drupal\advancedqueue\JobTypeManager;
use Drupal\advancedqueue\Processor as OriginalProcessor;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\sync_clients\Exceptions\SyncClientNotFoundException;
use Drupal\sync_clients\SyncClient\SyncClientManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Extends the Advanced Queue Processor with pause mechanism.
 */
class Processor extends OriginalProcessor {

  /**
   * Constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The current time.
   * @param \Drupal\advancedqueue\JobTypeManager $job_type_manager
   *   The queue job type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\sync_clients\SyncClient\SyncClientManagerInterface $syncClientManager
   *   The Sync Client Manager.
   */
  public function __construct(
    EventDispatcherInterface $event_dispatcher,
    TimeInterface $time,
    JobTypeManager $job_type_manager,
    LoggerInterface $logger,
    protected SyncClientManagerInterface $syncClientManager,
  ) {
    parent::__construct($event_dispatcher, $time, $job_type_manager, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public function processQueue(QueueInterface $queue): int {

    try {

      // Don't process outgoing data if the Sync Client is paused, whatever
      // the cause.
      // @todo We could allow for multiple clients per queue and allow to
      // proceed if at least one is not paused, is it is the Job Type that
      // determines the final Sync Client.
      $sync_client = $this->syncClientManager->getPluginByQueueId((string) $queue->id());
      if (!is_null($sync_client->isPaused())) {
        return 0;
      }
    }
    catch (SyncClientNotFoundException $exception) {
      // Silent fail.
    }

    return parent::processQueue($queue);
  }

}
