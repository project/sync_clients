<?php

declare(strict_types=1);

namespace Drupal\sync_clients;

use Drupal\advancedqueue\Job;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines helper to handle Advanced Queue items.
 */
interface QueueHelperInterface {

  /**
   * Add an entity to queue for given Job Type.
   *
   * Based on the Job Type, the corresponding Sync Client is found to
   * - generate the appropriate Job payload given the Entity
   * - find and add the Job to the corresponding Queue.
   *
   * @param string $job_type_id
   *   The Job Type ID.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The Entity.
   * @param array $payload_extra
   *   Extra data to merge into the payload for the Job.
   * @param int $delay
   *   The time, in seconds, after which the job will become available to
   *   queue consumers. Defaults to 0, indicating no delay.
   *
   * @throws \Drupal\advancedqueue\Exception\DuplicateJobException
   * @throws \Drupal\sync_clients\Exceptions\QueueNotFoundException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientNotFoundException
   * @throws \Drupal\sync_clients\Exceptions\UnsupportedEntityException
   * @throws \Exception
   *
   * @return \Drupal\advancedqueue\Job|null
   *   The Job or NULL if not created.
   */
  public function addToQueue(
    string $job_type_id,
    ?EntityInterface $entity = NULL,
    array $payload_extra = [],
    int $delay = 0,
  ): ?Job;

}
