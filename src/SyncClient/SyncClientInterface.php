<?php

declare(strict_types=1);

namespace Drupal\sync_clients\SyncClient;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\sync_clients\SyncClientPause\SyncClientPauseInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Defines Sync Clients.
 */
interface SyncClientInterface extends ConfigurableInterface, PluginInspectionInterface {

  /**
   * The config key.
   *
   * @var string
   */
  public const CONFIG_KEY_DEFAULT = 'sync_clients.settings';

  /**
   * The key in the config of the recipients for error mails.
   *
   * @var string
   */
  public const CONF_RECIPIENTS_ERROR = 'recipients_error';

  /**
   * The key in the config of the recipients for notice mails.
   *
   * @var string
   */
  public const CONF_RECIPIENTS_NOTICE = 'recipients_notice';

  /**
   * The key in the config of the recipients for warning mails.
   *
   * @var string
   */
  public const CONF_RECIPIENTS_WARNING = 'recipients_warning';

  /**
   * The KeyValueStore collection name for this module.
   *
   * @var string
   */
  public const KEYVALUE_COLLECTION = 'sync_clients';

  /**
   * The key in the keyvalue store of the pause duration.
   *
   * @var string
   */
  public const KEYVALUE_PAUSE_DURATION = 'pause_duration';

  /**
   * The mail type of error mails.
   *
   * @var string
   */
  public const MAIL_TYPE_ERROR = 'sync_error';

  /**
   * The mail type of notice mails.
   *
   * @var string
   */
  public const MAIL_TYPE_NOTICE = 'sync_notice';

  /**
   * The mail type of warning mails.
   *
   * @var string
   */
  public const MAIL_TYPE_WARNING = 'sync_warning';

  /**
   * The duration 'sync error level 1' pause state, in minutes.
   *
   * @var int
   */
  public const PAUSE_DURATION_LVL_1 = 1;

  /**
   * The duration 'sync error level 2' pause state, in minutes.
   *
   * @var int
   */
  public const PAUSE_DURATION_LVL_2 = 5;

  /**
   * The duration 'sync error level 2' pause state, in minutes.
   *
   * @var int
   */
  public const PAUSE_DURATION_LVL_3 = 10;

  /**
   * The state key for the paused state of the Sync Client.
   *
   * @var string
   */
  public const STATE_KEY_PAUSED = 'sync_clients_paused';

  /**
   * Add plugin specific fields to the Sync Client plugin config form.
   *
   * To retain values of password fields, add them to the form state storage
   * using their field name, and they will be copied over to the form values
   * in the form validation step.
   *
   * @param array $form
   *   The config form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function addPluginFields(array &$form, FormStateInterface $form_state): void;

  /**
   * Build a payload array for a (Advanced Queue) Job item.
   *
   * Warning: If you override this function, be sure to check, and update if
   * necessary, duplicateDetectionQueueItemPayloadKeys as well.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity.
   * @param array $payload_extra
   *   Extra data to merge into the payload for the Job.
   *
   * @return array
   *   The payload.
   *
   * @throws \Drupal\sync_clients\Exceptions\UnsupportedEntityException
   */
  public function buildQueueItemPayload(?EntityInterface $entity = NULL, array $payload_extra = []): array;

  /**
   * Get the payload keys to use for duplicate queue item detection.
   *
   * @return array
   *   The keys to use.
   */
  public function duplicateDetectionQueueItemPayloadKeys(): array;

  /**
   * Get the config key/name for the plugin.
   *
   * @return string
   *   The config key/name.
   */
  public function getConfigKey(): string;

  /**
   * Get the Job type that is using this client.
   *
   * @return string
   *   The Job Type ID.
   */
  public function getJobTypeId(): string;

  /**
   * Gets the Sync Client label.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The label.
   */
  public function getLabel(): TranslatableMarkup;

  /**
   * Get the logger.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getLogger(): LoggerInterface;

  /**
   * Set the logger.
   *
   * Override the default logger channel.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function setLogger(LoggerInterface $logger): void;

  /**
   * Get the Queue that is using this client.
   *
   * @return string
   *   The Queue ID.
   */
  public function getQueueId(): string;

  /**
   * Check if Maintenance Mode is active.
   *
   * @return bool
   *   TRUE if Maintenance Mode is on.
   */
  public function isMaintenanceMode(): bool;

  /**
   * Check if the Sync Client is paused (all requests are blocked).
   *
   * @param bool $skip_maintenance_check
   *   Whether to skip the check if maintenance mode is on. Allows to view the
   *   real pause reason when maintenance mode is active.
   *   Defaults to FALSE.
   * @param bool $skip_ping
   *   Whether to skip the ping check to see if the client can be unpaused.
   *   Defaults to FALSE.
   *
   * @return \Drupal\sync_clients\SyncClientPause\SyncClientPauseInterface|null
   *   The Sync Client Pause object or NULL if not paused.
   */
  public function isPaused(bool $skip_maintenance_check = FALSE, bool $skip_ping = FALSE): ?SyncClientPauseInterface;

  /**
   * Pause the Sync Client, thus blocking all requests to the remote.
   *
   * This function must implement the pause mechanism with the error levels.
   * This implies that if an error level is already active, anything other
   * than cause but 'manual', should be ignored and the level raising protocol
   * respected.
   * Unknown cause or 'maintenance' will be converted to 'forced'.
   *
   * @param string $cause
   *   The pause cause. Any of the PAUSE_CAUSE_[X] constants.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $reason
   *   Optional, but recommended, reason for why the sync was paused.
   */
  public function pause(string $cause, ?TranslatableMarkup $reason = NULL): void;

  /**
   * Forced pause the Sync Client, thus blocking all requests to the remote.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $reason
   *   Optional, but recommended, reason for why the sync was paused.
   */
  public function pauseForced(?TranslatableMarkup $reason = NULL): void;

  /**
   * Ping the remote to see if it is available to handle requests.
   *
   * @param bool $suppress_exceptions
   *   Whether to allow connection exceptions to bubble or not. If TRUE,
   *   exceptions will be caught and FALSE will be returned.
   *   Defaults to TRUE.
   *
   * @return bool
   *   TRUE if the remote is available.
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientInvalidCredentialsException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientPausedException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientRemoteBusyException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientUnknownException
   */
  public function ping(bool $suppress_exceptions = TRUE): bool;

  /**
   * Make request to the remote.
   *
   * This is merely a wrapper around the HTTP Client request method, to handle
   * the basic pause mechanism behaviour.
   *
   * @param string $method
   *   The HTTP method.
   * @param string $uri
   *   The URI.
   * @param array $options
   *   The request options to apply. See \GuzzleHttp\RequestOptions.
   * @param bool $bypass_pause
   *   Whether to bypass the pause mechanism. Defaults to FALSE.
   *   Use with care!
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientInvalidCredentialsException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientPausedException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientRemoteBusyException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientUnknownException
   */
  public function request(string $method, string $uri, array $options, bool $bypass_pause = FALSE): ResponseInterface;

  /**
   * Send error mail.
   *
   * To be used when an error has occurred that requires manual intervention
   * ASAP.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $subject
   *   The mail subject.
   * @param array $body
   *   The mail body, or messages.
   */
  public function sendErrorMail(TranslatableMarkup $subject, array $body): void;

  /**
   * Send notice mail.
   *
   * To be used when something unusual has occurred, which might need to be
   * looked into, or something noteworthy has happened.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $subject
   *   The mail subject.
   * @param array $body
   *   The mail body, or messages.
   */
  public function sendNoticeMail(TranslatableMarkup $subject, array $body): void;

  /**
   * Send warning mail.
   *
   * To be used when the sync is experiencing problems, that might automatically
   * be resolved, and no manual intervention is required (yet).
   * The goal is to inform the relevant persons, that an actual error might be
   * coming up soon.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $subject
   *   The mail subject.
   * @param array $body
   *   The mail body, or messages.
   */
  public function sendWarningMail(TranslatableMarkup $subject, array $body): void;

  /**
   * Test if given client config works for the API.
   *
   * @param array $config
   *   Client config to use.
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientInvalidCredentialsException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientPausedException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientRemoteBusyException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientUnknownException
   */
  public function testConfig(array $config): void;

  /**
   * Unpause the Sync Client, allowing requests to the remote.
   */
  public function unPause(): void;

  /**
   * Validate the plugin specific fields in the Sync Client plugin config form.
   *
   * @param array $form
   *   The config form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validatePluginFields(array &$form, FormStateInterface $form_state): void;

  /**
   * Convert a config array to a 'one item per line' string.
   *
   * @param array $array
   *   The config array.
   *
   * @return string
   *   The config string.
   */
  public static function configArrayToText(array $array): string;

  /**
   * Convert a config string (aka 'one item per line') to an array.
   *
   * @param string $string
   *   The config as string.
   *
   * @return array
   *   The config as array.
   */
  public static function configTextToArray(string $string): array;

}
