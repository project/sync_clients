<?php

declare(strict_types=1);

namespace Drupal\sync_clients\SyncClient;

use Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Manages Sync Job Type plugins.
 */
interface SyncClientManagerInterface extends PluginManagerInterface, CachedDiscoveryInterface, CacheableDependencyInterface {

  /**
   * Get Sync Client plugin by ID.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return \Drupal\sync_clients\SyncClient\SyncClientInterface
   *   The Sync Client plugin.
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientNotFoundException
   */
  public function getPluginById(string $plugin_id): SyncClientInterface;

  /**
   * Get Sync Client plugin for given (Advanced Queue) Job Type ID.
   *
   * @param string $job_type_id
   *   The (Advanced Queue) Job Type ID.
   *
   * @return \Drupal\sync_clients\SyncClient\SyncClientInterface
   *   The Sync Client plugin.
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientNotFoundException
   */
  public function getPluginByJobTypeId(string $job_type_id): SyncClientInterface;

  /**
   * Get Sync Client plugin for given (Advanced Queue) Queue ID.
   *
   * @param string $queue_id
   *   The (Advanced Queue) Queue ID.
   *
   * @return \Drupal\sync_clients\SyncClient\SyncClientInterface
   *   The Sync Client plugin.
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientNotFoundException
   */
  public function getPluginByQueueId(string $queue_id): SyncClientInterface;

}
