<?php

declare(strict_types=1);

namespace Drupal\sync_clients\SyncClient;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\sync_clients\Annotation\SyncClient;
use Drupal\sync_clients\Exceptions\SyncClientNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * Manages Sync Client plugins.
 */
final class SyncClientManager extends DefaultPluginManager implements SyncClientManagerInterface {

  /**
   * Cache of Sync Clients, keyed by their corresponding Queue ID.
   *
   * @var array
   */
  protected array $syncClients;

  /**
   * Constructs a new SyncClientPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    protected LoggerInterface $logger,
  ) {
    parent::__construct('Plugin/SyncClient', $namespaces, $module_handler, SyncClientInterface::class, SyncClient::class);
    $this->alterInfo('sync_clients_sync_client_plugin_info');
    $this->setCacheBackend($cache_backend, 'sync_clients_sync_client_plugins');
    $this->syncClients = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginById(string $plugin_id): SyncClientInterface {
    return $this->getPlugin('id', $plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginByJobTypeId(string $job_type_id): SyncClientInterface {
    return $this->getPlugin('job_type_id', $job_type_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginByQueueId(string $queue_id): SyncClientInterface {
    return $this->getPlugin('queue_id', $queue_id);
  }

  /**
   * Get a new instance of plugin for given plugin definition key and value.
   *
   * @param string $key
   *   The plugin definition key to search by.
   * @param string $value
   *   The value to search for.
   *
   * @return \Drupal\sync_clients\SyncClient\SyncClientInterface
   *   A plugin instance.
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientNotFoundException
   */
  protected function getPlugin(string $key, string $value): SyncClientInterface {

    try {

      $definitions = $this->getSortedDefinitions();
      foreach ($definitions as $definition) {

        if ($value === $definition[$key]) {

          $plugin_id = $definition['id'];
          if (isset($this->syncClients[$plugin_id])) {
            return $this->syncClients[$plugin_id];
          }

          /** @var \Drupal\sync_clients\SyncClient\SyncClientInterface  $plugin */
          $plugin = $this->createInstance($plugin_id);
          $this->syncClients[$plugin_id] = $plugin;

          return $plugin;
        }
      }
    }
    catch (PluginException $exception) {
      // Silent fail, handled below.
    }

    throw new SyncClientNotFoundException("No Sync Client found for $key = $value.");
  }

  /**
   * Gets the definition of all plugins for this type.
   *
   * @return array
   *   An array of plugin definitions (empty array if no definitions were
   *   found). Keys are plugin IDs.
   *
   * @see \Drupal\Core\Plugin\FilteredPluginManagerInterface::getFilteredDefinitions()
   */
  protected function getSortedDefinitions(): array {

    $definitions = $this->getDefinitions();

    // Sort the plugin definitions by plugin priority, with ID as fallback in
    // case of equal IDs.
    $id = array_column($definitions, 'id');
    $priority = array_column($definitions, 'priority');
    array_multisort($priority, SORT_DESC, $id, SORT_ASC, $definitions);

    return $definitions;
  }

}
