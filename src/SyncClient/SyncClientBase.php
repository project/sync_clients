<?php

declare(strict_types=1);

namespace Drupal\sync_clients\SyncClient;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\sync_clients\Exceptions\SyncClientInvalidCredentialsException;
use Drupal\sync_clients\Exceptions\SyncClientPausedException;
use Drupal\sync_clients\Exceptions\SyncClientRemoteBusyException;
use Drupal\sync_clients\Exceptions\SyncClientUnknownException;
use Drupal\sync_clients\Mail\MailHandlerInterface;
use Drupal\sync_clients\SyncClientPause\SyncClientPause;
use Drupal\sync_clients\SyncClientPause\SyncClientPauseInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Base implementation of the Sync Client.
 */
abstract class SyncClientBase extends PluginBase implements ContainerFactoryPluginInterface, SyncClientInterface {

  /**
   * The Sync Client default configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $configDefault;

  /**
   * The Sync Client plugin config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The expire-able key/value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected KeyValueStoreExpirableInterface $keyValueStore;

  /**
   * The Mail Handler.
   *
   * Not DI-ed, because it should hardly be used, or never if everything runs
   * perfectly.
   *
   * @var \Drupal\sync_clients\Mail\MailHandlerInterface
   */
  protected MailHandlerInterface $mailHandler;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   *   We want this private to enforce all plugins to make their requests using
   *   the request method provided by the base class, which implements the basic
   *   pause mechanism behaviour.
   *   However, this cause problems when the Sync Client needs to be serialized
   *   as a dependencies as core does not support private properties in
   *   dependency serialization. See core issue
   *   @link https://www.drupal.org/project/drupal/issues/3110266 @endlink.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $keyValueExpirable
   *   The expirable key/value factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected Client $httpClient,
    protected ConfigFactoryInterface $configFactory,
    protected KeyValueExpirableFactoryInterface $keyValueExpirable,
    protected LoggerInterface $logger,
    protected StateInterface $state,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configDefault = $configFactory->get(self::CONFIG_KEY_DEFAULT);
    $this->config = $configFactory->get($this->getConfigKey());
    $this->keyValueStore = $keyValueExpirable->get(static::KEYVALUE_COLLECTION);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('keyvalue.expirable'),
      $container->get('logger.channel.sync_clients'),
      $container->get('state')
    );
  }

  /**
   * Perform the actual ping request, to be handled by ping().
   *
   * Note: make sure to call request() with $bypass_pause on TRUE.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientInvalidCredentialsException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientPausedException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientRemoteBusyException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientUnknownException
   */
  abstract protected function doPing(): ResponseInterface;

  /**
   * {@inheritdoc}
   */
  public function addPluginFields(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function buildQueueItemPayload(?EntityInterface $entity = NULL, array $payload_extra = []): array {

    $payload = [];

    if (!is_null($entity)) {
      $payload = [
        'bundle' => $entity->bundle(),
        'entity_type' => $entity->getEntityTypeId(),
        'id' => $entity->id(),
      ];
    }

    if (count($payload_extra) > 0) {
      $payload = NestedArray::mergeDeep($payload, $payload_extra);
    }

    return $payload;
  }

  /**
   * {@inheritdoc}
   */
  public function duplicateDetectionQueueItemPayloadKeys(): array {
    return [
      'bundle',
      'entity_type',
      'id',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigKey(): string {
    return SyncClientInterface::CONFIG_KEY_DEFAULT . ".{$this->getPluginId()}";
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep(
      $this->baseConfigurationDefaults(),
      $this->defaultConfiguration(),
      $this->config->get(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getJobTypeId(): string {
    return $this->pluginDefinition['job_type_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): TranslatableMarkup {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLogger(): LoggerInterface {
    return $this->logger;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(LoggerInterface $logger): void {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueueId(): string {
    return $this->pluginDefinition['queue_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function isMaintenanceMode(): bool {
    return (bool) $this->state->get('system.maintenance_mode');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function isPaused(bool $skip_maintenance_check = FALSE, bool $skip_ping = FALSE): ?SyncClientPauseInterface {

    // Always pause when Maintenance Mode is on.
    if ($skip_maintenance_check === FALSE && $this->isMaintenanceMode() === TRUE) {
      return new SyncClientPause(SyncClientPauseInterface::PAUSE_CAUSE_MAINTENANCE, $this->t('Maintenance Mode is active.'));
    }

    $state = $this->getPauseState();
    $cause_is_limitless = (!is_null($state) && ($state->isForced() || $state->isManual()));
    $duration = $this->getPauseDuration();

    // Duration will expire, but not the pause level.
    if (is_null($duration) && !is_null($state) && !$cause_is_limitless) {
      $this->state->delete($this->getPauseStateKey());
      $state = NULL;
    }

    if (!is_null($state) && !$cause_is_limitless) {

      // If duration has passed, do connection test. If it succeeds, we lift
      // the pause state.
      // If it fails, we pause again to raise the error level.
      if (is_null($duration) && $skip_ping === FALSE) {

        if ($this->ping()) {

          $this->unPause();
          $error_level = str_replace('_', ' ', $state->getCause());

          $context = [
            'error_level' => $error_level,
            'label' => $this->getLabel(),
          ];
          $this->logger->notice('Sync has recovered from {error_level}.', $context);

          $params = [
            '@error_level' => $error_level,
            '@label' => $this->getLabel(),
          ];

          $subject = $this->t("Sync '@label' has recovered from @error_level", $params);
          $body = [
            $this->t("Sync '@label' has recovered from @error_level. No intervention is required.", $params),
            $this->t('This mail has been sent for informative purposes only.', $params),
          ];

          $this->sendNoticeMail($subject, $body);
        }
        else {
          // Start over.
          $this->pause(SyncClientPauseInterface::PAUSE_CAUSE_ERROR_LVL_1, $this->t('Ping not successful.'));
        }
      }
    }

    return $state;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function pause(string $cause, ?TranslatableMarkup $reason = NULL): void {

    if (is_null($reason)) {
      $reason = $this->t('No reason provided.');
    }

    $causes_error_levels = [
      SyncClientPauseInterface::PAUSE_CAUSE_ERROR_LVL_1 => static::PAUSE_DURATION_LVL_1,
      SyncClientPauseInterface::PAUSE_CAUSE_ERROR_LVL_2 => static::PAUSE_DURATION_LVL_2,
      SyncClientPauseInterface::PAUSE_CAUSE_ERROR_LVL_3 => static::PAUSE_DURATION_LVL_3,
    ];
    $causes_limitless = [
      SyncClientPauseInterface::PAUSE_CAUSE_MAINTENANCE,
      SyncClientPauseInterface::PAUSE_CAUSE_FORCED,
      SyncClientPauseInterface::PAUSE_CAUSE_MANUAL,
    ];

    // If given cause does not match any of our defined constants, consider it
    // 'forced'.
    if (!in_array($cause, $causes_limitless, TRUE) && !in_array($cause, array_keys($causes_error_levels), TRUE)) {
      $cause = SyncClientPauseInterface::PAUSE_CAUSE_FORCED;
    }

    // Maintenance should not be se by code, so convert it to 'forced'.
    if ($cause === SyncClientPauseInterface::PAUSE_CAUSE_MAINTENANCE) {
      $cause = SyncClientPauseInterface::PAUSE_CAUSE_FORCED;
    }

    // For logging.
    $context = [
      'error_level' => $cause,
      'label' => $this->getLabel(),
      'reason' => $reason,
    ];

    // For mails.
    $params = [
      '@label' => $this->getLabel(),
    ];

    // Error level raising mechanism.
    if (in_array($cause, array_keys($causes_error_levels), TRUE)) {

      // If we already are in pause state, there are a few things to consider.
      // If there is none, we can just set it with its duration.
      $state = $this->getPauseState();
      if (is_null($state)) {

        $this->setPauseState($cause, $reason);
        $this->setPauseDuration($cause, $causes_error_levels[$cause]);
      }
      else {

        // We do have an active pause state.
        // First, the limitless pause states overrule the expirable, so we don't
        // need to change the state.
        if (in_array($state->getCause(), $causes_limitless, TRUE)) {

          // This should not happen if our overall logic is sound.
          // However, log this, so we know if it happens, which might indicate
          // a gap in our logic.
          $this->logger->notice("Pause called on '{label}' when already in limitless pause state. {error_level}: {reason}.", $context);

          return;
        }

        // Ending up here means there is an active error level, whose duration
        // may or may not have expired.
        // If the duration has expired and the pause function was called, it
        // means a request was made to check if the connection works again, and
        // it failed. Thus, we must raise the error level.
        $duration = $this->getPauseDuration();
        if (is_null($duration)) {

          $level_indexes = array_flip(array_keys($causes_error_levels));
          $count = count($level_indexes);
          $index = $level_indexes[$state->getCause()];

          // Increase the error level.
          $index++;

          // Max error level has been exceeded, we go to 'forced' state.
          if ($index >= $count) {

            $this->setPauseState(SyncClientPauseInterface::PAUSE_CAUSE_FORCED, $reason);
            $this->clearPauseDuration();

            $subject = $this->t("Sync '@label' has been forcefully paused, manual intervention is required", $params);
            $this->sendErrorMail($subject, [$reason]);
          }
          else {

            // Update the cause with corresponding the new level.
            $level_indexes = array_flip($level_indexes);
            $cause = $level_indexes[$index];

            $this->setPauseState($cause, $reason);
            $this->setPauseDuration($cause, $causes_error_levels[$cause]);

            // Highest error level has been reached, also send a warning mail
            // that next failure will cause a 'forced' pause.
            if ($index === ($count - 1)) {

              $subject = $this->t("Sync '@label' has reached error level 3.", $params);
              $params['@duration'] = $causes_error_levels[$cause];

              $body = [
                $this->t("Sync '@label' has reached error level 3. If next connection attempt fails in about @duration minutes, it will go in forced pause state and manual intervention will be required", $params),
                $this->t('However, if the next attempt succeeds, normal processing will continue.'),
              ];
              $this->sendWarningMail($subject, $body);
            }
          }
        }

        // If the duration has not expired.
        else {

          // This should not happen either if our overall logic is sound.
          // However, log this, so we know if it happens, which might indicate
          // a gap in our logic.
          $context = [
            'error_level' => $cause,
            'reason' => $reason,
          ];
          $this->logger->notice("Pause called on '{label}' when still in expire-able pause state. {error_level}: {reason}.", $context);
        }
      }
    }
    // Ending up here means one of the limitless pause states was given.
    // So just set the state.
    else {
      $this->setPauseState($cause, $reason);

      $subject = $cause === SyncClientPauseInterface::PAUSE_CAUSE_MANUAL ?
        $this->t("Sync '@label' has been manually paused.", $params) :
        $this->t("Sync '@label' has been forcefully paused.", $params);
      $this->sendErrorMail($subject, [$reason]);
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function pauseForced(?TranslatableMarkup $reason = NULL): void {
    $this->pause(SyncClientPauseInterface::PAUSE_CAUSE_FORCED, $reason);
  }

  /**
   * {@inheritdoc}
   */
  public function ping(bool $suppress_exceptions = TRUE): bool {

    try {

      $response = $this->doPing();
      return $response->getStatusCode() === Response::HTTP_OK;
    }
    catch (\Exception $exception) {

      if ($suppress_exceptions) {
        return FALSE;
      }

      throw $exception;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function request(string $method, string $uri, array $options, bool $bypass_pause = FALSE): ResponseInterface {

    // Make sure we're not paused before attempting anything.
    $paused = $this->isPaused();
    if (!is_null($paused)
      && $paused->getCause() !== SyncClientPauseInterface::PAUSE_CAUSE_MAINTENANCE
      && $bypass_pause === FALSE) {
      throw new SyncClientPausedException($paused->getPauseMessage()->render());
    }

    try {
      return $this->httpClient->request($method, $uri, $options);
    }
    catch (ClientException | ServerException $exception) {

      $message = $exception->getMessage();
      if (trim($message) === '') {

        $previous = $exception->getPrevious();
        if ($previous instanceof \Throwable) {
          $message = $previous->getMessage();
        }
      }

      // Some 4xx, 5xx code we can handle different.
      switch ($exception->getResponse()->getStatusCode()) {

        case Response::HTTP_UNAUTHORIZED:
          $this->pauseForced($this->t('Invalid API credentials.'));
          throw new SyncClientInvalidCredentialsException($message, $exception->getCode(), $exception);

        case Response::HTTP_REQUEST_TIMEOUT:
        case 503:
        case 504:
          $this->pause(SyncClientPauseInterface::PAUSE_CAUSE_ERROR_LVL_1, $this->t('Request timeout'));
          throw new SyncClientRemoteBusyException($message, $exception->getCode(), $exception);

        default:
          // Fall through.
      }

      throw new SyncClientUnknownException($message, $exception->getCode(), $exception);
    }
    catch (ConnectException | TooManyRedirectsException $exception) {

      $message = $exception->getMessage();
      if (trim($message) !== '') {
        $message = $exception->getPrevious()->getMessage();
      }

      $this->pauseForced($this->t('Could not connect to the remote. Is the API url correct?'));
      throw new SyncClientInvalidCredentialsException($message, $exception->getCode(), $exception);
    }
    catch (GuzzleException $exception) {

      $message = $exception->getMessage();
      if (trim($message) !== '') {
        $message = $exception->getPrevious()->getMessage();
      }

      throw new SyncClientUnknownException($message, $exception->getCode(), $exception);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendErrorMail(TranslatableMarkup $subject, array $body): void {

    $to = $this->configuration[static::CONF_RECIPIENTS_ERROR];
    $this->sendMail(static::MAIL_TYPE_ERROR, $to, $subject, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function sendNoticeMail(TranslatableMarkup $subject, array $body): void {

    $to = $this->configuration[static::CONF_RECIPIENTS_NOTICE];
    $this->sendMail(static::MAIL_TYPE_NOTICE, $to, $subject, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function sendWarningMail(TranslatableMarkup $subject, array $body): void {

    $to = $this->configuration[static::CONF_RECIPIENTS_WARNING];
    $this->sendMail(static::MAIL_TYPE_WARNING, $to, $subject, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function testConfig(array $config = []): void {

    // Keep current stored config safe.
    $config_backup = $this->configuration;
    $this->configuration = $config;

    $this->ping(FALSE);

    // Restore original config.
    $this->configuration = $config_backup;
  }

  /**
   * {@inheritdoc}
   */
  public function unPause(): void {
    $this->state->delete($this->getPauseStateKey());
    $this->clearPauseDuration();
  }

  /**
   * {@inheritdoc}
   */
  public function validatePluginFields(array &$form, FormStateInterface $form_state): void {}

  /*
   * Helper functions.
   */

  /**
   * {@inheritdoc}
   */
  public static function configArrayToText(array $array): string {
    return implode("\r\n", $array);
  }

  /**
   * {@inheritdoc}
   */
  public static function configTextToArray($string): array {

    $string = trim($string);
    if ($string === '') {
      return [];
    }

    return explode("\r\n", $string);
  }

  /**
   * Returns global Sync Client config.
   *
   * @return array|null
   *   An associative array with the default configuration.
   */
  protected function baseConfigurationDefaults(): ?array {
    return $this->configDefault->get();
  }

  /**
   * Sends an email using our Mail Handler.
   *
   * Both success and failure will be logged.
   *
   * @param string $type
   *   The mail type/id.
   * @param string $to
   *   The address the email will be sent to. Must comply with RFC 2822.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $subject
   *   The subject. Must not contain any newline characters.
   * @param array $body
   *   A render array representing the message body.
   * @param array $params
   *   Email parameters. Recognized keys:
   *     - id: A unique identifier of the email type.
   *       Allows hook_mail_alter() implementations to identify specific emails.
   *       Defaults to "mail". Automatically prefixed with "commerce_".
   *     - from: The address the email will be marked as being from.
   *       Defaults to the current store email.
   *     - reply-to: The address to which the reply will be sent. No default.
   *     - cc: The CC address or addresses (separated by a comma). No default.
   *     - bcc: The BCC address or addresses (separated by a comma). No default.
   *     - langcode: The email langcode. Every translatable string and entity
   *       will be rendered in this language. Defaults to the current language.
   */
  protected function sendMail(string $type, string $to, TranslatableMarkup $subject, array $body, array $params = []): void {

    $subject = $subject->render();
    $subject_prefix = NULL;
    switch ($type) {
      case static::MAIL_TYPE_ERROR:
        $subject_prefix = $this->t('Error');
        break;

      case static::MAIL_TYPE_NOTICE:
        $subject_prefix = $this->t('Notice');
        break;

      case static::MAIL_TYPE_WARNING:
        $subject_prefix = $this->t('Warning');
        break;
    }
    if ($subject_prefix instanceof TranslatableMarkup) {
      $subject = "{$subject_prefix->render()}: $subject";
    }

    if (trim($to) === '') {
      return;
    }

    // Handle recipients from config.
    $to = static::configTextToArray($to);
    $to = implode(',', $to);

    $body = [
      '#theme' => 'sync_clients_mail',
      '#type' => $type,
      '#items' => $body,
      '#sync_client_label' => $this->getLabel(),
    ];

    if (isset($params['id']) && $params['id'] === '') {
      $params['id'] = $type;
    }

    if (!isset($this->mailHandler)) {
      $this->mailHandler = \Drupal::service('sync_clients.mail_handler');
    }

    $this->mailHandler->sendMail($to, $subject, $body, $params);
  }

  /*
   * Pause state handling.
   */

  /**
   * Get the pause state.
   *
   * @return \Drupal\sync_clients\SyncClientPause\SyncClientPauseInterface|null
   *   The Sync Client Pause object.
   */
  protected function getPauseState(): ?SyncClientPauseInterface {
    $state = $this->state->get($this->getPauseStateKey());

    if (!is_null($state)) {
      $state = unserialize($state);
    }

    return $state;
  }

  /**
   * Get the state key for the pause value of current plugin.
   *
   * @return string
   *   The state key.
   */
  protected function getPauseStateKey(): string {
    return static::STATE_KEY_PAUSED . "_{$this->getPluginId()}";
  }

  /**
   * Set the pause state.
   *
   * @param string $cause
   *   The pause cause. Any of the PAUSE_CAUSE_[X] constants.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $reason
   *   The pause reason.
   *
   * @throws \Exception
   */
  protected function setPauseState(string $cause, TranslatableMarkup $reason): void {
    $state = new SyncClientPause($cause, $reason);
    $state = serialize($state);
    $this->state->set($this->getPauseStateKey(), $state);
  }

  /**
   * Get the pause duration.
   *
   * @return string|null
   *   The corresponding error level.
   */
  protected function getPauseDuration(): ?string {
    return $this->keyValueStore->get($this->getPauseDurationKey());
  }

  /**
   * Get the keyvaluestore key for the pause duration of current plugin.
   *
   * @return string
   *   The keyvaluestore key.
   */
  protected function getPauseDurationKey(): string {
    return static::KEYVALUE_PAUSE_DURATION . "_{$this->getPluginId()}";
  }

  /**
   * Set the pause duration for given error level.
   *
   * @param string $value
   *   The corresponding error level.
   * @param int $expire
   *   The time to live for items, in minutes.
   */
  protected function setPauseDuration(string $value, int $expire): void {
    $expire = $expire * 60;
    $this->keyValueStore->setWithExpire($this->getPauseDurationKey(), $value, $expire);
  }

  /**
   * Clear the pause duration, if set.
   */
  protected function clearPauseDuration(): void {
    $this->keyValueStore->delete($this->getPauseDurationKey());
  }

}
