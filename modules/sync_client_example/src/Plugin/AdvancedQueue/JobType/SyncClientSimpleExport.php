<?php

declare(strict_types=1);

namespace Drupal\sync_client_example\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\sync_clients\Exceptions\SyncClientPausedException;
use Drupal\sync_clients\Exceptions\SyncClientRemoteBusyException;
use Drupal\sync_clients\Plugin\AdvancedQueue\JobType\SyncJobBase;

/**
 * Simple Export JobType corresponding with the SyncClientSimpleExport.
 *
 * @AdvancedQueueJobType(
 *   id = "sync_clients_simple_export",
 *   label = @Translation("Sync Clients: Simple Export"),
 *   max_retries = 60,
 *   retry_delay = 60
 *   allow_duplicates = false
 * )
 */
class SyncClientSimpleExport extends SyncJobBase {

  /**
   * {@inheritdoc}
   */
  public function process(Job $job): JobResult {

    // Let the parent figure out the appropriate Sync Client. If that already
    // fails, then just pass it on.
    $job_result = parent::process($job);
    if ($job_result->getState() === Job::STATE_FAILURE) {
      return $job_result;
    }

    try {

      // @todo Prepare data for your need using $payload.
      $payload = $job->getPayload();
      $data = [];

      /** @var \Drupal\sync_client_example\Plugin\SyncClient\SyncClientSimpleExport $sync_client */
      $sync_client = $this->syncClient;
      $sync_client->post($data);
    }
    // Retry with default retry delay. Next try has high chance of succeeding.
    catch (SyncClientRemoteBusyException $exception) {

      // No sync status update either, as still pending.
      return JobResult::failure("Remote busy: {$exception->getMessage()} Try again later.");
    }
    // This can happen if the sync gets paused between the start of this job
    // and making the actual request: Retry with zero delay, so it gets picked
    // up again in its current place and don't count this try.
    catch (SyncClientPausedException $exception) {

      // No sync status update either, as still pending.
      return JobResult::failure('Sync has been paused during processing. Try again later.', NULL, 0);
    }
    // All other exceptions: Return as hard-fail, no retries.
    catch (\Exception $exception) {
      return JobResult::failure($exception->getMessage(), 0, 0);
    }

    return JobResult::success();
  }

}
