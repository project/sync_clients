<?php

declare(strict_types=1);

namespace Drupal\sync_client_example\Plugin\SyncClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sync_clients\SyncClient\SyncClientBase;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Example Sync Client corresponding with SyncClientExampleJobtype.
 *
 * @SyncClient(
 *   id = "sync_clients_simple_export",
 *   label = @Translation("Sync Clients: Simple Export"),
 *   priority = 1,
 *   queue_id = "sync_clients_simple_export",
 *   job_type_id = "sync_clients_simple_export"
 * )
 */
class SyncClientSimpleExport extends SyncClientBase {

  /**
   * {@inheritdoc}
   */
  public function addPluginFields(array &$form, FormStateInterface $form_state): void {

    // @todo Add any other value you need to be configurable.
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REST Endpoint URL'),
      '#description' => $this->t('The full URL of the endpoint.'),
      '#default_value' => $this->config->get('url') ?? '',
      '#required' => TRUE,
    ];
  }

  /**
   * POST data to the remote.
   *
   * @param array $data
   *   The data.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   *
   * @throws \Drupal\sync_clients\Exceptions\SyncClientInvalidCredentialsException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientPausedException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientRemoteBusyException
   * @throws \Drupal\sync_clients\Exceptions\SyncClientUnknownException
   */
  public function post(array $data): ResponseInterface {
    return $this->request(Request::METHOD_POST, $this->configuration['url'], $this->buildOptions($data));
  }

  /**
   * Build the options for the request.
   *
   * @param array $data
   *   The request data.
   *
   * @return array
   *   The request options.
   */
  protected function buildOptions(array $data = []): array {

    $options = [
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
        'Cache-Control' => 'no-cache',
        'Content-Type' => 'application/json',
      ],
    ];

    $options[RequestOptions::BODY] = json_encode($data);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function doPing(): ResponseInterface {
    return $this->request(Request::METHOD_GET, $this->configuration['url'], $this->buildOptions(), TRUE);
  }

}
